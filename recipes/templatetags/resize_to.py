from django import template

register = template.Library()


# def resize_to(ingredientAmount, servings):
#     if ingredientAmount is not None:
#         try:
#             ingAmt = int(ingredientAmount)
#             s = int(0 if servings is None else servings)
#             newIngredient = ingAmt * s
#             if newIngredient > 0:
#                 return newIngredient
#         except ValueError:
#             pass
#     return ingredientAmount


# register.filter(resize_to)


def resize_to(ingredient, target):
    servings = ingredient.recipe.servings
    if servings is not None and target is not None:
        try:
            ratio = int(target) / int(servings)
            return ingredient.amount * ratio
        except ValueError:
            pass
    return ingredient.amount


register.filter(resize_to)
