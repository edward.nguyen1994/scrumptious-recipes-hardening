from django.urls import path

from recipes.views import *

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("shopping_items/", list_shopping_items, name="shopping_items_list"),
    path(
        "shopping_items/create/",
        create_shopping_item,
        name="shopping_item_create",
    ),
    path(
        "shopping_items/delete/",
        delete_all_shopping_items,
        name="delete_all_shopping_items",
    ),
]

# path( 1-use this pattern to match URLs with request,
#       2-when URL matches call this fxn in views.py,
#       3-the name that is used in redirect, reverse, and {% url %} tags)
