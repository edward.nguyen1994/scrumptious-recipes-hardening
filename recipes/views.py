from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError

from recipes.forms import RatingForm

# try:
# from recipes.forms import RecipeForm
from recipes.models import *

# except Exception:
#     RecipeForm = None
#     Recipe = None


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            try:
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 10


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        # Create a new empty list and assign it to a variable
        foods = []

        # For each item in the user's shopping items, which we
        # can access through the code
        # self.request.user.shopping_items.all()
        for item in self.request.user.shopping_item.all():
            # Add the shopping item's food to the list
            foods.append(item.food_item)

        # The self.request.GET property is a dictionary
        # Get the value out of there associated with the
        #   key "servings"
        # Store in the context dictionary with the key
        #   "servings"
        context["servings"] = self.request.GET.get("servingss")

        # Put that list into the context
        context["food_in_shopping_list"] = foods

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "servings", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "servings", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


@login_required
def list_shopping_items(request):
    context = {
        "ShoppingItems_list": ShoppingItem.objects.filter(user=request.user),
    }
    return render(request, "shopping_items/list.html", context)
    # why didn't recipes/shopping_items/list.html work???
    # because recipes in recipes/list.html is the template, not the app
    # so shopping_items in shopping_items/list.html is the template
    # so no need to callout recipes app


@login_required
def create_shopping_item(request):
    # Get the ingredient_id from the POST
    ingredient_id = request.POST.get("ingredient_id")
    # ingredient_id is defined in detail.html, form

    # Get the specific ingredient from the Ingredient model
    ingredient = Ingredient.objects.get(id=ingredient_id)

    # Get the current user
    user = request.user
    try:
        # Create the new shopping item in the database
        ShoppingItem.objects.create(
            food_item=ingredient.food,
            user=user,
        )
    # Catch the error if its already in there
    except IntegrityError:
        # Don't do anything with the error
        pass

    # Go back to the recipe page
    return redirect("recipe_detail", pk=ingredient.recipe.id)


@login_required
def delete_all_shopping_items(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_items_list")
