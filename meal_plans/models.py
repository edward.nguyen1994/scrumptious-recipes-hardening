from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.


class MealPlan(models.Model):
    name = models.CharField(max_length=120, null=True)
    date = models.DateField(null=True)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plans",
        # why and when is related_name necessary ??
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans"
    )

    def __str__(self):
        return self.name + " by " + str(self.owner)
